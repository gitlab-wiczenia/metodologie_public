#include <iostream>
using namespace std;



int check_if_word_palindrome(string pal)
{
    for (int i = 0; i < pal.size() / 2; i++)
    {
        if (pal[i] != pal[pal.size() - 1 - i])
        {
            return 1;
        }
    }
    
    return 0;
}
