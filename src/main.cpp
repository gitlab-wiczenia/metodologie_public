// import palindrome; //
#include "../src/palindrome.cpp"
#include <iostream>
#include <string>
using namespace std;

int main () {

   string test_string;
   cout << "Wpisz slowo aby sprawdzic czy jest palindromem" << endl;
   cin >> test_string ;

   if ( check_if_word_palindrome(test_string) == 0)
   {
      cout << "Podane slowo JEST palindromem" << endl;
   }
   else
   {
      cout << "Podane slowo NIE JEST palindromem" << endl;
   }

   return 0;
}
