// import palindrome; //
#include "../src/palindrome.cpp"
#include <iostream>
#include <string>
using namespace std;

int main () {
   string pal, nonpal;
   pal = "kajak";
   nonpal = "ola";
   if ( check_if_word_palindrome(pal) == 0 & check_if_word_palindrome(nonpal) == 1) {
      return 0;
   }
   else { return 1;}

   return 0; // Should not be executed
}
